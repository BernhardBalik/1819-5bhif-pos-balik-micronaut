package at.spengergasse.controller;

import at.spengergasse.domain.Artist;
import at.spengergasse.repository.ArtistRepository;
import io.micronaut.http.HttpHeaders;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.*;
import io.micronaut.validation.Validated;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

@Validated
@Controller("/artists")
public class ArtistController {
    protected final ArtistRepository artistRepository;

    public ArtistController(ArtistRepository artistRepository) {
        this.artistRepository = artistRepository;
    }

    @Get("/{id}")
    public Artist findById(Long id) {
        return artistRepository
                .findById(id)
                .orElse(null);
    }
    
    @Put("/")
    public HttpResponse update(@Body @Valid Artist artist) {
        int numberOfEntitiesUpdated = artistRepository.update(artist);

        return HttpResponse
                .noContent()
                .header(HttpHeaders.LOCATION, location(artist.getId()).getPath());
    }

    @Get
    public List<Artist> findAll() {
        return artistRepository.findAll();
    }

    @Post("/")
    public HttpResponse<Artist> save(@Body @Valid Artist artist) {
        Artist savedartist = artistRepository.save(artist);

        return HttpResponse
                .created(savedartist)
                .headers(headers -> headers.location(location(artist.getId())));
    }

    @Delete("/{id}")
    public HttpResponse delete(Long id) {
        artistRepository.deleteById(id);
        return HttpResponse.noContent();
    }

    protected URI location(Long id) {
        return URI.create("/artists/" + id);
    }

    protected URI location(Artist artist) {
        return location(artist.getId());
    }
}
