package at.spengergasse.controller;

import at.spengergasse.domain.Artist;
import at.spengergasse.domain.Track;
import at.spengergasse.repository.TrackRepository;
import io.micronaut.http.HttpHeaders;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.*;
import io.micronaut.validation.Validated;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

@Validated
@Controller("/tracks")
public class TrackController {
    protected final TrackRepository trackRepository;

    public TrackController(TrackRepository trackRepository) {
        this.trackRepository = trackRepository;
    }

    @Get("/{id}")
    public Track findById(Long id) {
        return trackRepository
                .findById(id)
                .orElse(null);
    }

    @Get("/artist")
    public List<Track> findByArtist(@Body @Valid Artist artist) {
        return trackRepository
                .findByArtist(artist);
    }

    @Put("/")
    public HttpResponse update(@Body @Valid Track track) {
        int numberOfEntitiesUpdated = trackRepository.update(track);

        return HttpResponse
                .noContent()
                .header(HttpHeaders.LOCATION, location(track.getId()).getPath());
    }

    @Get
    public List<Track> findAll() {
        return trackRepository.findAll();
    }

    @Post("/")
    public HttpResponse<Track> save(@Body @Valid Track track) {
        Track savedtrack = trackRepository.save(track);

        return HttpResponse
                .created(savedtrack)
                .headers(headers -> headers.location(location(savedtrack.getId())));
    }

    @Delete("/{id}")
    public HttpResponse delete(Long id) {
        trackRepository.deleteById(id);
        return HttpResponse.noContent();
    }

    protected URI location(Long id) {
        return URI.create("/tracks/" + id);
    }

    protected URI location(Track track) {
        return location(track.getId());
    }
}
