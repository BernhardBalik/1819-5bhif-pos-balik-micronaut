package at.spengergasse.repository;

import at.spengergasse.domain.Artist;
import io.micronaut.configuration.hibernate.jpa.scope.CurrentSession;
import io.micronaut.runtime.ApplicationConfiguration;
import io.micronaut.spring.tx.annotation.Transactional;

import javax.inject.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

@Singleton
public class ArtistReposioryImpl implements ArtistRepository{

    @PersistenceContext
    private EntityManager entityManager;
    private final ApplicationConfiguration applicationConfiguration;

    public ArtistReposioryImpl(@CurrentSession EntityManager entityManager,
                               ApplicationConfiguration applicationConfiguration) {
        this.entityManager = entityManager;
        this.applicationConfiguration = applicationConfiguration;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Artist> findAll() {
        String qlString = "select a from Artist as a";
        TypedQuery<Artist> query = entityManager.createQuery(qlString, Artist.class);
        return query.getResultList();
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Artist> findById(@NotNull Long id) {
        return Optional.ofNullable(entityManager.find(Artist.class, id));
    }

    @Override
    @Transactional
    public Artist save(Artist artist) {
        entityManager.persist(artist);
        return artist;
    }

    @Override
    @Transactional
    public int update(Artist artist) {
        return entityManager.createQuery("UPDATE Artist a SET name = :name, age = :age, gender = :gender where id = :id")
                .setParameter("name", artist.getName())
                .setParameter("age", artist.getAge())
                .setParameter("gender", artist.getGender())
                .setParameter("id", artist.getId())
                .executeUpdate();
    }

    @Override
    @Transactional
    public void deleteById(@NotNull Long id) {
        findById(id).ifPresent(artist -> entityManager.remove(artist));
    }
}
