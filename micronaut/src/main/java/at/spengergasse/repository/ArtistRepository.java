package at.spengergasse.repository;

import at.spengergasse.domain.Artist;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

public interface ArtistRepository {
    List<Artist> findAll();

    Optional<Artist> findById(@NotNull Long id);

    Artist save(Artist artist);

    int update(Artist artist);

    void deleteById(@NotNull Long id);
}
