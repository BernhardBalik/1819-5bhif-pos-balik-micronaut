package at.spengergasse.repository;

import at.spengergasse.domain.Artist;
import at.spengergasse.domain.Track;
import io.micronaut.configuration.hibernate.jpa.scope.CurrentSession;
import io.micronaut.runtime.ApplicationConfiguration;
import io.micronaut.spring.tx.annotation.Transactional;

import javax.inject.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

@Singleton
public class TrackReposioryImpl implements TrackRepository {

    @PersistenceContext
    private EntityManager entityManager;
    private final ApplicationConfiguration applicationConfiguration;

    public TrackReposioryImpl(@CurrentSession EntityManager entityManager,
                               ApplicationConfiguration applicationConfiguration) {
        this.entityManager = entityManager;
        this.applicationConfiguration = applicationConfiguration;
    }


    @Override
    @Transactional(readOnly = true)
    public List<Track> findAll() {
        String qlString = "select t from Track as t";
        TypedQuery<Track> query = entityManager.createQuery(qlString, Track.class);
        return query.getResultList();
    }

    @Override
    @Transactional(readOnly = true)
    public List<Track> findByArtist(Artist artist) {
        String qlString = "select t from Track as t where artist = :artist";
        TypedQuery<Track> query = entityManager.createQuery(qlString, Track.class).setParameter("artist", artist);
        return query.getResultList();
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Track> findById(@NotNull Long id) {
        return Optional.ofNullable(entityManager.find(Track.class, id));
    }

    @Override
    @Transactional
    public Track save(Track track) {
        entityManager.persist(track);
        return track;
    }

    @Override
    @Transactional
    public int update(Track track) {
        return entityManager.createQuery("UPDATE Track t SET title = :title, duration = :duration where id = :id")
                .setParameter("title", track.getTitle())
                .setParameter("duration", track.getDuration())
                .setParameter("id", track.getId())
                .executeUpdate();
    }

    @Override
    @Transactional
    public void deleteById(@NotNull Long id) {
        findById(id).ifPresent(track -> entityManager.remove(track));
    }
}
