package at.spengergasse.repository;

import at.spengergasse.domain.Artist;
import at.spengergasse.domain.Track;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

public interface TrackRepository {
    List<Track> findAll();

    List<Track> findByArtist(Artist artist);

    Optional<Track> findById(@NotNull Long id);

    Track save(Track track);

    int update(Track track);

    void deleteById(@NotNull Long id);
}
